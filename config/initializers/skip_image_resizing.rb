if Rails.env.rest?
  CarrierWave.configure do |config|
    config.enable_processing = false
  end
end